#include "Functions.h"
#include <iostream>


class X {
public:
	friend std::ostream& operator<<(std::ostream& os, const X& dt);
	X(int a) {
		_a = a;
	}

public:
	int _a;

};
std::ostream& operator<<(std::ostream& os, const X& a)
{
	os << a._a;
	return os;
}

inline bool operator==(const X& lhs, const X& rhs) { if (lhs._a==rhs._a) return true; else return false; }
inline bool operator!=(const X& lhs, const X& rhs) { return !operator==(lhs, rhs); }
inline bool operator< (const X& lhs, const X& rhs) { if (lhs._a < rhs._a) return true; else return false;}
inline bool operator> (const X& lhs, const X& rhs) { return  operator< (rhs, lhs); }
inline bool operator<=(const X& lhs, const X& rhs) { return !operator> (lhs, rhs); }
inline bool operator>=(const X& lhs, const X& rhs) { return !operator< (lhs, rhs); }





int main() {

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	std::cout << compare<char>('f', 'g') << std::endl;
	std::cout << compare<char>('k', 'a') << std::endl;
	std::cout << compare<char>('b', 'b') << std::endl;

	std::cout << compare<X>(1, 2) << std::endl;
	std::cout << compare<X>(4, 2) << std::endl;
	std::cout << compare<X>(4, 4) << std::endl;

	std::cout << "\033[1;32m \nCompare -- OK\033[0m\n \n" << std::endl;

	//check bubbleSort
	std::cout << "Expecting a sorted array: " << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	const int arr_size2 = 5;
	char charArr[arr_size2] = { 'Z', 'd', 'Q', 'a', 'b' };
	bubbleSort<char>(charArr, arr_size2);
	for (int i = 0; i < arr_size2; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	const int arr_size3 = 5;
	X XArr[arr_size3] = { 3, 6, 1, 7, 45 };
	bubbleSort<X>(XArr, arr_size3);
	for (int i = 0; i < arr_size3; i++) {
		std::cout << XArr[i] << " ";
	}
	std::cout << "\033[1;32m \n \nBubble Sort -- OK\033[0m\n \n" << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	printArray<char>(charArr, arr_size2);
	std::cout << std::endl;
	printArray<X>(XArr, arr_size2);

	std::cout << "\033[1;32m \nPrint Array -- OK\033[0m\n \n" << std::endl;
	system("pause");
	return 0;
}