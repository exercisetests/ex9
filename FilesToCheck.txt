##RequiredFiles
BSNode.h
BSNode.cpp
BSNode.hpp
Functions.h
main.cpp
BinaryTree.exe
printTreeToFile.h
printTreeToFile.lib
Device.h
Device.cpp
User.h
User.cpp
Page.h
Page.cpp
Profile.h
Profile.cpp
GenericList.hpp
testList.cpp

##RequiredVS
*[1].sln
*[1].vcxproj
*[1].vcxproj.filters

##Bonus

##ExcludedFiles
*.vcxproj.user
Debug
.vs