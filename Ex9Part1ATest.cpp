#include "BSNode.h"
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;

std::string globalTreeString = "";

std::string getBSTNodeInfo(const BSNode& t)
{
	std::string result = "{ data: \"" + t.getData() + "\" <" + std::to_string(t.getCount()) + ">, ";
	std::string sonData = "";
	if (t.getLeft())
		sonData = "\"" + t.getLeft()->getData() + "\" <" + std::to_string(t.getCount()) + ">";
	else
		sonData = "NULL";

	result += "left: " + sonData + " ,";

	if (t.getRight())
		sonData = "\""+ t.getRight()->getData() + "\" <" + std::to_string(t.getCount()) + ">";
	else
		sonData = "NULL";

	result += "right: " + sonData + " }";
	return result;
}

void convertBTToString(const std::string& prefix, const BSNode* node, bool isLeft, bool isAbsoluteRoot)
{
	if (node)
	{
		globalTreeString += prefix;
		if (isAbsoluteRoot)
		{
			globalTreeString = "|--";
		}
		else
			globalTreeString += (isLeft ? "[L]--" : "[R]--");

		// print the value of the node
		globalTreeString += node->getData() + ",<" + std::to_string(node->getCount()) + ">\n";

		// enter the next tree level - left and right branch
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getLeft(), true, false);
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getRight(), false, false);
	}
}

void convertBTToString(const BSNode* node)
{
	convertBTToString("", node, false, true);
}

int test1aBasicBST()
{

	try
	{
		// Tests Ex9 part 1a - basic binary tree

		cout <<
			"***********************************\n" <<
			"Test 1a - Basic Binary Search Tree \n" <<
			"***********************************\n" << endl;

		cout <<
			"Trying to initialize a new Binary Search Tree t1(\"Hello\") ... \n" << endl;

		BSNode t1("Hello");

		std::string expected = "{ data: \"Hello\" <1>, left: NULL ,right: NULL }";
		std::string got = getBSTNodeInfo(t1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9101");
			std::cout << " \n" << endl;
			return 9101;
		}

		cout <<
			"\nTrying to insert new value to tree t1.insert(\"Shalom\") ... \n" << endl;

		t1.insert("Shalom");
		expected = "{ data: \"Hello\" <1>, left: NULL ,right: \"Shalom\" <1> }";
		got = getBSTNodeInfo(t1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9102");
			std::cout << " \n" << endl;
			return 9102;
		}

		cout <<
			"\nTrying to insert new value to tree t1.insert(\"Bekef\") ... \n" << endl;

		t1.insert("Bekef");
		expected = "{ data: \"Hello\" <1>, left: \"Bekef\" <1> ,right: \"Shalom\" <1> }";
		got = getBSTNodeInfo(t1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9102");
			std::cout << " \n" << endl;
			return 9102;
		}

		cout <<
			"\nInserting values [\"AAA\",\"ZZZ\",\"CCC\",\"YYY\"] ... \n" << endl;

		t1.insert("AAA");
		t1.insert("ZZZ");
		t1.insert("CCC");
		t1.insert("YYY");

		convertBTToString(&t1);
		got = globalTreeString;

		expected = "|--Hello,<1>\n";
		expected += "    [L]--Bekef,<1>\n";
		expected += "       [L]--AAA,<1>\n";
		expected += "       [R]--CCC,<1>\n";
		expected += "    [R]--Shalom,<1>\n";
		expected += "        [R]--ZZZ,<1>\n";
		expected += "            [L]--YYY,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9102");
			std::cout << " \n" << endl;
			return 9102;
		}

		cout <<
			"\nInserting values [\"Bekef\",\"CCC\",\"CCC\",\"Hello\",\"YYY\",\"YYY\",\"YYY\"] ... \n" << endl;

		t1.insert("Bekef");
		t1.insert("CCC");
		t1.insert("CCC");
		t1.insert("Hello");
		t1.insert("YYY");
		t1.insert("YYY");
		t1.insert("YYY");

		convertBTToString(&t1);
		got = globalTreeString;

		expected = "|--Hello,<2>\n";
		expected += "    [L]--Bekef,<2>\n";
		expected += "       [L]--AAA,<1>\n";
		expected += "       [R]--CCC,<3>\n";
		expected += "    [R]--Shalom,<1>\n";
		expected += "        [R]--ZZZ,<1>\n";
		expected += "            [L]--YYY,<4>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9103");
			std::cout << " \n" << endl;
			return 9103;
		}
	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"################################################\n" <<
		"Exercise 9 - Binary Search Trees and Templates\n" <<
		"Part 1a - Basic Binary Search Tree\n" <<
		"################################################\n" << std::endl;

	int testResult = test1aBasicBST();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex9 Part 1A Tests Passed ******\033[0m\n \n" : "\033[1;31mEx9 Part 1A Tests Failed\033[0m\n \n") << endl;

	return testResult;
}