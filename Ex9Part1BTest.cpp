#include "BSNode.h"
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;

std::string globalTreeString = "";

void convertBTToString(const std::string& prefix, const BSNode* node, bool isLeft, bool isAbsoluteRoot)
{
	if (node)
	{
		globalTreeString += prefix;
		if (isAbsoluteRoot)
		{
			globalTreeString = "|--";
		}
		else
			globalTreeString += (isLeft ? "[L]--" : "[R]--");

		// print the value of the node
		globalTreeString += node->getData() + ",<" + std::to_string(node->getCount()) + ">\n";

		// enter the next tree level - left and right branch
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getLeft(), true, false);
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getRight(), false, false);
	}
}

void convertBTToString(const BSNode* node)
{
	convertBTToString("", node, false, true);
}

int test2bCopyBST()
{
	try
	{
		// Tests Ex9 part 1b - copy binary search tree

		cout <<
			"***********************************\n" <<
			"Test 1b - Binary Search Tree Copy \n" <<
			"***********************************\n" << endl;

		cout <<
			"Initializing a new tree t1(\"MMM\")... \n" << endl;

		BSNode t1("MMM");
		cout << "\033[1;32mOK\033[0m\n \n" << endl;

		cout <<
			"\nInserting values to t1 [\"DDD\",\"LLL\",\"VVV\",\"VVV\",\"BBB\",\"KKK\",\"TTT\"] ... \n" << endl;

		t1.insert("DDD");
		t1.insert("LLL");
		t1.insert("VVV");
		t1.insert("VVV");
		t1.insert("BBB");
		t1.insert("KKK");
		t1.insert("TTT");

		convertBTToString(&t1);
		std::string got = globalTreeString;

		std::string expected = "";
		expected = "|--MMM,<1>\n";
		expected += "    [L]--DDD,<1>\n";
		expected += "       [L]--BBB,<1>\n";
		expected += "       [R]--LLL,<1>\n";
		expected += "           [L]--KKK,<1>\n";
		expected += "    [R]--VVV,<2>\n";
		expected += "        [L]--TTT,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9103");
			std::cout << " \n" << endl;
			return 9103;
		}

		cout <<
			"\nInitializing a tree using copy constructor -- BSNode t2(t1) ... \n" << endl;
		BSNode t2(t1);

		convertBTToString(&t2);
		got = globalTreeString;

		expected = "|--MMM,<1>\n";
		expected += "    [L]--DDD,<1>\n";
		expected += "       [L]--BBB,<1>\n";
		expected += "       [R]--LLL,<1>\n";
		expected += "           [L]--KKK,<1>\n";
		expected += "    [R]--VVV,<2>\n";
		expected += "        [L]--TTT,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9104");
			std::cout << " \n" << endl;
			return 9104;
		}

		cout <<
			"\nInserting elements to t1 - [\"DDD\",\"AAA\"] ... \n" << endl;

		t1.insert("DDD");
		t1.insert("AAA");

		convertBTToString(&t1);
		got = globalTreeString;

		expected = "|--MMM,<1>\n";
		expected += "    [L]--DDD,<2>\n";
		expected += "       [L]--BBB,<1>\n";
		expected += "          [L]--AAA,<1>\n";
		expected += "       [R]--LLL,<1>\n";
		expected += "           [L]--KKK,<1>\n";
		expected += "    [R]--VVV,<2>\n";
		expected += "        [L]--TTT,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9103");
			std::cout << " \n" << endl;
			return 9103;
		}

		cout <<
			"\Checking t2 (should be unchanged) ... \n" << endl;

		convertBTToString(&t2);
		got = globalTreeString;

		expected = "|--MMM,<1>\n";
		expected += "    [L]--DDD,<1>\n";
		expected += "       [L]--BBB,<1>\n";
		expected += "       [R]--LLL,<1>\n";
		expected += "           [L]--KKK,<1>\n";
		expected += "    [R]--VVV,<2>\n";
		expected += "        [L]--TTT,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9105");
			std::cout << " \n" << endl;
			return 9105;
		}

		cout <<
			"\nUsing copy operator t2 = t1 ... \n" << endl;

		t2 = t1;

		cout <<
			"\nPrinting t2 again (should be like t1) ... \n" << endl;

		convertBTToString(&t2);
		got = globalTreeString;

		expected = "|--MMM,<1>\n";
		expected += "    [L]--DDD,<2>\n";
		expected += "       [L]--BBB,<1>\n";
		expected += "          [L]--AAA,<1>\n";
		expected += "       [R]--LLL,<1>\n";
		expected += "           [L]--KKK,<1>\n";
		expected += "    [R]--VVV,<2>\n";
		expected += "        [L]--TTT,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9106");
			std::cout << " \n" << endl;
			return 9106;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"################################################\n" <<
		"Exercise 9 - Binary Search Trees and Templates\n" <<
		"Part 1b - Copy Binary Search Tree\n" <<
		"################################################\n" << std::endl;

	int testResult = test2bCopyBST();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex9 Part 1B Tests Passed ******\033[0m\n \n" : "\033[1;31mEx9 Part 1B Tests Failed\033[0m\n \n") << endl;

	return testResult;
}