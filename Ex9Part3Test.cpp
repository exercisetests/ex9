#include "BSNode.hpp"
#include <string>
#include <vector>

using std::cout;
using std::endl;

std::string globalTreeString = "";

template<class T>
std::string getBSTNodeInfo(const BSNode<T>& t)
{
	std::string result = "{ data: \"" + t.getData() + "\" <" + std::to_string(t.getCount()) + ">, ";
	std::string sonData = "";
	if (t.getLeft())
		sonData = "\"" + t.getLeft()->getData() + "\" <" + std::to_string(t.getCount()) + ">";
	else
		sonData = "NULL";

	result += "left: " + sonData + " ,";

	if (t.getRight())
		sonData = "\""+ t.getRight()->getData() + "\" <" + std::to_string(t.getCount()) + ">";
	else
		sonData = "NULL";

	result += "right: " + sonData + " }";
	return result;
}

template<class T>
void convertBTToString(const std::string& prefix, const BSNode<T>* node, bool isLeft, bool isAbsoluteRoot)
{
	if (node)
	{
		globalTreeString += prefix;
		if (isAbsoluteRoot)
		{
			globalTreeString = "|--";
		}
		else
			globalTreeString += (isLeft ? "[L]--" : "[R]--");

		// print the value of the node
		globalTreeString += node->getData() + ",<" + std::to_string(node->getCount()) + ">\n";

		// enter the next tree level - left and right branch
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getLeft(), true, false);
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getRight(), false, false);
	}
}

template<class T>
void convertBTToString(const BSNode<T>* node)
{
	convertBTToString("", node, false, true);
}

bool test5TemplateBST()
{
	bool result = false;

	try
	{
		// Tests Ex9 part 3 - binary search tree with templates

		cout <<
			"***************************************\n" <<
			"Test 1a - Generic Binary Search Tree \n" <<
			"***************************************\n" << endl;

		cout <<
			"Trying to initialize a new Binary Search Tree for std::strings t1(\"TTT\") ... \n" << endl;

		BSNode<std::string> t1("Imagine");

		cout << "\033[1;32mOK\033[0m\n \n" << endl;

		cout <<
			"\nInserting values [\"All\",\"The\",\"People\",\"Living\",\"Life\",\"In\",";
		cout<<
			"\"Peace\",\"WhoohooOooooo\",\"You\",\"May\",\"Say\",\"I'm\",\"A\",\"Dreamer\",";
		cout <<
			"\"But\",\"I'm\",\"Not\",\"The\",\"Only\",\"One\"] ... \n" << endl;

		t1.insert("All");
		t1.insert("The");
		t1.insert("People");
		t1.insert("Living");
		t1.insert("Life");
		t1.insert("In");
		t1.insert("Peace");
		t1.insert("WhoohooOooooo");
		t1.insert("You");
		t1.insert("May");
		t1.insert("Say");
		t1.insert("I'm");
		t1.insert("A");
		t1.insert("Dreamer");
		t1.insert("But");
		t1.insert("I'm");
		t1.insert("Not");
		t1.insert("The");
		t1.insert("Only");
		t1.insert("One");

		convertBTToString(&t1);
		std::string got = globalTreeString;

		std::string expected;
		expected = "|--Imagine,<1>\n";
		expected += "    [L]--All,<1>\n";
		expected += "       [L]--A,<1>\n";
		expected += "       [R]--I'm,<2>\n";
		expected += "           [L]--Dreamer,<1>\n";
		expected += "              [L]--But,<1>\n";
		expected += "    [R]--The,<2>\n";
		expected += "        [L]--People,<1>\n";
		expected += "           [L]--Living,<1>\n";
		expected += "              [L]--Life,<1>\n";
		expected += "                 [L]--In,<1>\n";
		expected += "              [R]--Peace,<1>\n";
		expected += "                  [L]--May,<1>\n";
		expected += "                     [R]--Not,<1>\n";
		expected += "                         [R]--Only,<1>\n";
		expected += "                             [L]--One,<1>\n";
		expected += "           [R]--Say,<1>\n";
		expected += "        [R]--WhoohooOooooo,<1>\n";
		expected += "            [R]--You,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9301");
			std::cout << " \n" << endl;
			return 9301;
		}

		cout <<
			"\nInitializing a new binary search tree for integers ... \n" << endl;

		const int arr_size = 15;
		int intArr[arr_size] = { 6,24,74,-54,0,24,67,34,67,789,34,234,57,787,34 };

		cout << "\nInserting values [";

		for (int i = 0; i < arr_size - 1; i++) {
			std::cout << intArr[i] << ", ";
		}
		std::cout << intArr[arr_size - 1] << "]" << std::endl;

		BSNode<int> bs(intArr[0]);
		for (int i = 1; i < arr_size; i++) {
			bs.insert(intArr[i]);
		}

		cout << "\033[1;32mOK\033[0m\n \n" << endl;

		cout << "\nPrinting Tree using BSNode<T>::printNodes\n" << std::endl;;
		bs.printNodes();

		cout << "\033[1;32m \nOK\033[0m\n \n" << endl;

		cout <<
			"\nCalculating t2 height \"AAA\" -- t2.getHeight() ... \n" << endl;
		expected = "5";
		got = std::to_string(bs.getHeight());
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9302");
			std::cout << " \n" << endl;
			return 9302;
		}

		cout <<
			"\nCalculating the depth of node \"8\" from t2 root ... \n" << endl;
		
		BSNode<int>* bs1 = bs.getRight()->getRight()->getLeft()->getLeft();

		expected = "4";
		got = std::to_string(bs1->getDepth(bs));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9303");
			std::cout << " \n" << endl;
			return 9303;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"################################################\n" <<
		"Exercise 9 - Binary Search Trees and Templates\n" <<
		"Part 3 - Generic Binary Search Tree\n" <<
		"################################################\n" << std::endl;

	int testResult = test5TemplateBST();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex9 Part 3 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx9 Part 3 Tests Failed\033[0m\n \n") << endl;

	return testResult;
}