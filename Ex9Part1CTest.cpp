#include "BSNode.h"
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;

std::string globalTreeString = "";

void convertBTToString(const std::string& prefix, const BSNode* node, bool isLeft, bool isAbsoluteRoot)
{
	if (node)
	{
		globalTreeString += prefix;
		if (isAbsoluteRoot)
		{
			globalTreeString = "|--";
		}
		else
			globalTreeString += (isLeft ? "[L]--" : "[R]--");

		// print the value of the node
		globalTreeString += node->getData() + ",<" + std::to_string(node->getCount()) + ">\n";

		// enter the next tree level - left and right branch
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getLeft(), true, false);
		convertBTToString(prefix + (isLeft ? "   " : "    "), node->getRight(), false, false);
	}
}

void convertBTToString(const BSNode* node)
{
	convertBTToString("", node, false, true);
}

int test3BSTUtils()
{
	try
	{
		// Tests Ex9 part 1c - binary search tree advanced test

		cout <<
			"***********************************\n" <<
			"Test 1c - Binary Search Tree Utils \n" <<
			"***********************************\n" << endl;

		cout <<
			"Initializing a new tree t1(\"MMM\")... \n" << endl;

		BSNode t1("MMM");
		cout << "\033[1;32mOK\033[0m\n \n" << endl;

		cout <<
			"\nInserting values [\"DDD\",\"LLL\",\"VVV\",\"VVV\",\"BBB\",\"KKK\",\"TTT\"] ... \n" << endl;

		t1.insert("DDD");
		t1.insert("LLL");
		t1.insert("VVV");
		t1.insert("VVV");
		t1.insert("BBB");
		t1.insert("KKK");
		t1.insert("TTT");

		convertBTToString(&t1);
		std::string got = globalTreeString;

		std::string expected = "";
		expected = "|--MMM,<1>\n";
		expected += "    [L]--DDD,<1>\n";
		expected += "       [L]--BBB,<1>\n";
		expected += "       [R]--LLL,<1>\n";
		expected += "           [L]--KKK,<1>\n";
		expected += "    [R]--VVV,<2>\n";
		expected += "        [L]--TTT,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9103");
			std::cout << " \n" << endl;
			return 9103;
		}

		/////////////////////
		// checking search //
		/////////////////////

		cout <<
			"\nSearching for value \"AAA\" -- t1.search(\"AAA\") ... \n" << endl;

		t1.search("AAA");
		expected = "false";
		got = t1.search("AAA") ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9107");
			std::cout << " \n" << endl;
			return 9107;
		}

		cout <<
			"\nSearching for value \"VVV\" -- t1.search(\"VVV\") ... \n" << endl;


		t1.search("VVV");
		expected = "true";
		got = t1.search("VVV") ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9108");
			std::cout << " \n" << endl;
			return 9108;
		}

		cout <<
			"\nSearching for value \"MMM\" -- t1.search(\"MMM\") ... \n" << endl;

		t1.search("MMM");
		expected = "true";
		got = t1.search("MMM") ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9109");
			std::cout << " \n" << endl;
			return 9109;
		}

		cout <<
			"\nInserting elements to t1 - [\"DDD\",\"AAA\"] ... \n" << endl;

		t1.insert("DDD");
		t1.insert("AAA");

		convertBTToString(&t1);
		got = globalTreeString;

		expected = "|--MMM,<1>\n";
		expected += "    [L]--DDD,<2>\n";
		expected += "       [L]--BBB,<1>\n";
		expected += "          [L]--AAA,<1>\n";
		expected += "       [R]--LLL,<1>\n";
		expected += "           [L]--KKK,<1>\n";
		expected += "    [R]--VVV,<2>\n";
		expected += "        [L]--TTT,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9103");
			std::cout << " \n" << endl;
			return 9103;
		}

		cout <<
			"\nSearching for value \"AAA\" -- t1.search(\"AAA\") ... \n" << endl;

		t1.search("AAA");
		expected = "true";
		got = t1.search("AAA") ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9110");
			std::cout << " \n" << endl;
			return 9110;
		}

		////////////////////////////
		// checking height & depth//
		////////////////////////////

		cout <<
			"\nCalculating t1 height -- t1.getHeight() ... \n" << endl;

		expected = "3";
		got = std::to_string(t1.getHeight());
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9111");
			std::cout << " \n" << endl;
			return 9111;
		}

		cout <<
			"\nCalculating the depth of node \"KKK\" from t1 root ... \n" << endl;
		BSNode* bs = t1.getLeft()->getRight()->getLeft();

		expected = "3";
		got = std::to_string(bs->getDepth(t1));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9112");
			std::cout << " \n" << endl;
			return 9112;
		}

		cout <<
			"\nChecking if node \"KKK\" is a leaf on t1 ... \n" << endl;

		expected = "true";
		got = bs->isLeaf() ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9113");
			std::cout << " \n" << endl;
			return 9113;
		}

		cout <<
			"\nChecking if node \"LLL\" is a leaf on t1 ... \n" << endl;

		bs = t1.getLeft()->getRight();
		expected = "false";
		got = bs->isLeaf() ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9114");
			std::cout << " \n" << endl;
			return 9114;
		}

		cout <<
			"\nCalculating the depth of node \"TTT\" from t1 root ... \n" << endl;
		bs = t1.getRight()->getLeft();

		expected = "2";
		got = std::to_string(bs->getDepth(t1));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9115");
			std::cout << " \n" << endl;
			return 9115;
		}

		cout <<
			"\nChecking if node \"TTT\" is a leaf on t1 ... \n" << endl;

		expected = "true";
		got = bs->isLeaf() ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9116");
			std::cout << " \n" << endl;
			return 9116;
		}

		cout <<
			"\nInitializing a new tree t2(\"1\")... \n" << endl;

		BSNode t2("1");
		cout << "\033[1;32mOK\033[0m\n \n" << endl;

		cout <<
			"\nInserting values [\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\"] ... \n" << endl;

		t2.insert("2");
		t2.insert("3");
		t2.insert("4");
		t2.insert("5");
		t2.insert("6");
		t2.insert("7");
		t2.insert("8");

		convertBTToString(&t2);
		got = globalTreeString;

		expected = "|--1,<1>\n";
		expected += "    [R]--2,<1>\n";
		expected += "        [R]--3,<1>\n";
		expected += "            [R]--4,<1>\n";
		expected += "                [R]--5,<1>\n";
		expected += "                    [R]--6,<1>\n";
		expected += "                        [R]--7,<1>\n";
		expected += "                            [R]--8,<1>\n";

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9103");
			std::cout << " \n" << endl;
			return 9103;
		}

		cout <<
			"\nCalculating t2 height \"AAA\" -- t2.getHeight() ... \n" << endl;

		expected = "7";
		got = std::to_string(t2.getHeight());
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9117");
			std::cout << " \n" << endl;
			return 9117;
		}

		cout <<
			"\nCalculating the depth of node \"8\" from t2 root ... \n" << endl;
		
		bs = &t2;
		for(unsigned int i = 0; i < 7; i++)
			bs = bs->getRight();

		expected = "7";
		got = std::to_string(bs->getDepth(t2));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9118");
			std::cout << " \n" << endl;
			return 9118;
		}

		cout <<
			"\nChecking if node \"8\" is a leaf on t2 ... \n" << endl;

		expected = "true";
		got = bs->isLeaf() ? "true" : "false";
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 9119");
			std::cout << " \n" << endl;
			return 9119;
		}

		return 0;
	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}
}

int main()
{
	std::cout <<
		"################################################\n" <<
		"Exercise 9 - Binary Search Trees and Templates\n" <<
		"Part 1c - Binary Search Tree Utils\n" <<
		"################################################\n" << std::endl;

	int testResult = test3BSTUtils();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex9 Part 1C Tests Passed ******\033[0m\n \n" : "\033[1;31mEx9 Part 1C Tests Failed\033[0m\n \n") << endl;

	return testResult;
}